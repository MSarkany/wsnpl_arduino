### A stripped-down port of WSNProtectLayer for Arduino-based JeeLink devices
#### Features:
  - AES encryption and MAC for secure communication
  - μTESLA authenticated broadcast
  - Neighbor discovery
  - Simplified CTP protocol
  - Configurator for nodes' IDs and pairwise keys
